Test data for neuroracer_ai unittests

- Copy left_right_bag_for_unittest.bag to neuroracer_ai/training_data/rosbags/train and neuroracer_ai/training_data/rosbags/test
- Copy steering_default_mono8-tf-128x72-crop_t10_b90_unittest.hdf5 to neuroracer_ai/trained_data 